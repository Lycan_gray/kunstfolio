<?php

use Illuminate\Http\Request;


Route::post('/users/login', 'AuthController@login');
Route::post('/users/register', 'AuthController@register');

Route::get('/user/{user_slug}', 'UserController@User');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('interests', 'InterestsController@getInterests');
    Route::get('announcements', 'AnnouncementsController@getAnnouncements');
    Route::get('assignments', 'AssignmentsController@getAssignments');
    Route::get('assignments/{key}', 'AssignmentsController@getClassAssignments');

    // User Specific Routes
    Route::prefix('users')->group(function () {
        Route::get('/details', 'UserController@details');
        Route::post('profilepicture', 'UserController@profilePictureUpload');
        Route:: post('update', 'UserController@updateUser');
        Route::get('interests', 'InterestsController@getUserInterests');
        Route:: post('interests', 'InterestsController@setUserInterests');
        Route::get('assignment/{key}', 'AssignmentsController@getUserAssignments');
        Route::post('assignment/{key}', 'AssignmentsController@uploadUserAssignment');
        Route::get('calendarevents', 'ApiController@getUserCalendarEvents');
        Route ::post('/changepassword', 'AuthController@changePassword');

    });
});
