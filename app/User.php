<?php

    namespace App;

    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Laravel\Passport\HasApiTokens;
    use Spatie\Sluggable\HasSlug;
    use Spatie\Sluggable\SlugOptions;

    class User extends Authenticatable
    {
        use Notifiable, HasApiTokens, HasSlug;

        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable
            = [
                'name',
                'email',
                'password',
                'address',
                'postalcode',
                'dob',
                'avatar',
                'preference',
                'housenumber',
                'phonenumber',
                'user_slug',
                'city',
            ];

        /**
         * The attributes that should be hidden for arrays.
         *
         * @var array
         */
        protected $hidden
            = [
                'password',
                'remember_token',
            ];

        /**
         * The attributes that should be cast to native types.
         *
         * @var array
         */
        protected $casts
            = [
                'email_verified_at' => 'datetime',
            ];

        public
        function interests()
        {
            return $this -> belongsToMany(Interests::class, 'user_interest');
        }

        public function userAssignments() {
            return $this -> hasMany(UserAssignments::class);
        }
        public function getSlugOptions(): SlugOptions
        {
            return SlugOptions::create()
                ->generateSlugsFrom(['name', 'id'])
                ->saveSlugsTo('user_slug');
        }

        public function calendarEvents() {
            return $this -> hasMany(CalendarEvent::class);
        }
    }
