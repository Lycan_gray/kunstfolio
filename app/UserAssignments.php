<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAssignments extends Model
{
    protected $fillable = ['assignment_id', 'user_id', 'user_upload', 'self_assessment_text', 'self_assessment_grade', 'created_at', 'updated_at'];
    public function parentAssignment() {
        return $this -> hasOne(Assignment::class, 'assignment_id');
    }
}
