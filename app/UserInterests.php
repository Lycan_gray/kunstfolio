<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInterests extends Model
{
    protected $table = 'user_interests';
    protected $fillable = ['interests_key','user_id', 'interests_id'];
}
