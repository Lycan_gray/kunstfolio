<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{
    public function classCode()
    {
        return $this->belongsTo('App\ClassCode');
    }

}
