<?php

    namespace App\Http\Controllers;

    use App\User;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\Hash;
    use Validator;

    class AuthController extends Controller
    {

        /**
         * login api
         *
         * @return \Illuminate\Http\Response
         */
        public function login(Request $request)
        {
            $credentials = $request->only('email', 'password');
            $authInstance = Auth::attempt($credentials);
            if ($authInstance) {
                $user = Auth::user();
                $success['token'] = $user->createToken($credentials['email'])->accessToken;

                $responseJson = [
                    'id' => $user->id,
                    'token' => $success['token']
                ];
                return response()->json($responseJson, 200);
            } else {
                return response()->json('Username or password is incorrect', 403);
            }
        }
        /**
         * Register api
         *
         * @return \Illuminate\Http\Response
         */
        public function register(Request $request)
        {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'address' => 'required',
                'postalcode' => 'required',
                'email' => 'required|email',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            $input = $request->all();
            $input['password'] = bcrypt($input['password']);

            $user = User::create($input);


            return response()->json(['success' => $user], 200);
        }

        /**
         * ChangePassword api
         *
         * @return \Illuminate\Http\Response
         */
        public function changePassword(Request $request){
            /** @var User $user */
            $user = User::find($request->user()->id);
            $currentPass = $request->currentPass;
            if (Hash::check($currentPass, $user->getAuthPassword())){
                $newPass = Hash::make($request->newPass);
                $user->update(['password' => $newPass]);
                $user->save();
                return response()->json(['success' => 'Password has been changed!'], 200);
            } else {
                return response('Wrong Password!', 401);
            }
        }
    }
