<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Resources\User as UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    public function User(Request $request)
    {
        $user_slug = $request->user_slug;

        $user = User::where('user_slug', '=', $user_slug)->get()->first();
        return response()->json($user, 200);
    }

    public function details()
    {
        return response()->json(request()->user(), 200);
//        $user = new UserResource(request()->user());
//        return response()->json($user, 200);
    }

    public function updateUser(Request $request)
    {
        /** @var User $user */
        $user = User::where('id', '=', $request->user()->id)->firstOrFail();
        $user->update($request->toArray());
        return response()->json('User updated');
    }

    public function profilePictureUpload(Request $request)
    {

        $request->validate([
            'profile_picture' => 'required|file|mimes:jpeg,png,jpg,gif'
        ]);
        /** @var User $user */
        $user = User::where('id', '=', $request->user()->id)->firstOrFail();

        if ($request->has('profile_picture')) {
            $profilepicture = $request->file('profile_picture');

            $res = Storage::put('public/images/avatars', $profilepicture, 'public');
            $url = Storage::url($res);

            $user->avatar = asset($url);
            $user->save();
            return response('Success');
        } else {
            return response($request->toArray(), 400);
        }


    }

}
