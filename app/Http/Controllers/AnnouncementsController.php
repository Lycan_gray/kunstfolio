<?php

namespace App\Http\Controllers;

use App\ClassCode;

class AnnouncementsController extends Controller
{
    public function getAnnouncements()
    {
        $announcements = ClassCode::with(['Announcements'])->get();
        return response()->json($announcements, 200);
    }
}
