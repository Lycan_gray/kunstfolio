<?php

namespace App\Http\Controllers;

use App\Interests;
use App\UserInterests;
use Illuminate\Http\Request;

class InterestsController extends Controller
{

    public function getInterests()
    {
        $interests = Interests::get();
        return response()->json($interests, 200);
    }

    public function getUserInterests()
    {
        $userInterests = request()->user()->interests()->get();
        return response()->json($userInterests, 200);
    }

    public function setUserInterests()
    {

        $uploadData = [
            'interests_key' => request()->interests_key,
            'user_id' => request()->user()->id,
            'interests_id' => request()->interests_id
        ];
        $userInterests = UserInterests::updateOrCreate(
            ['interests_key' => request()->interests_key, 'id' => request()->user()->id],
            $uploadData
        );

        return response()->json('SUCCESS', 200);
    }
}
