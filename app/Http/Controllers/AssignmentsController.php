<?php

namespace App\Http\Controllers;

use App\Http\Resources\Assignment as AssignmentsResource;
use App\ClassCode;
use App\Assignment;
use App\TeacherAssignments;
use App\UserAssignments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AssignmentsController extends Controller
{
    public function getUserAssignments($key)
    {
        $assignment = request()->user()->userAssignments()->where('assignment_id', $key)->firstOrFail();
        $teacher_end = TeacherAssignments::where('assignment_id', $assignment->id)->first();
//        if($teacher) {
//            $teacher_end = $teacher;
//        }else{
//            $teacher_end = null;
//        }
        return response()->json(['user_assignment' => $assignment, 'teacher_assignment' => $teacher_end], 200);
    }

    public function getClassAssignments($key)
    {
        $assignments = ClassCode::with(['Assignments'])->where('class_code', $key)->get();
        return response()->json($assignments, 200);
    }


    public function getAssignments()
    {
        $assignments = ClassCode::with(['Assignments'])->get();
        return response()->json($assignments, 200);
    }

    public function uploadUserAssignment(Request $request, $key)
    {
        $requestData = array_filter(\request()->all());


        $uploadData = [];
        foreach ($requestData as $arrKey => $value) {
            $uploadData[$arrKey] = $value;
        }
        if (!is_string($uploadData['user_upload'])) {
            $rules = [
                'user_upload' => 'required|file|mimes:jpeg,png,jpg|max:2000',
                'self_assessment_text' => 'min:10',
                'self_assessment_grade' => 'integer|min:0|max:10'
            ];
        } else {
          $rules = [
              'self_assessment_text' => 'min:10',
              'self_assessment_grade' => 'integer|min:0|max:10'
          ];
        }

        $messages = [
            'mimes' => 'Ongeldig bestandstype. Alleen: jpg, png en jpeg',
            'max' => 'Bestand te groot, Max 2 mb',
            'self_assessment_text.min' => 'Minimaal aantal karakters: 10',
            'self_assessment_grade.min' => 'Cijfer kan niet lager dan 0 zijn',
            'self_assessment_grade.max' => 'Cijfer kan niet hoger dan 10 zijn'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(["message" => $errors, "error" => true], 200);
        }

        if (!is_string($uploadData['user_upload'])) {
            $path = $uploadData['user_upload']->store('public/images/user-uploads');
            $imagePath = preg_replace('/public/', url('/') . '/storage', $path);
            $uploadData['user_upload'] = $imagePath;
        } else {
            unset($uploadData['user_upload']);
        }

        $userAssignment = UserAssignments::updateOrCreate(
            ['assignment_id' => $key, 'user_id' => request()->user()->id],
            $uploadData
        );

        return response()->json($userAssignment, 200);
    }

}
