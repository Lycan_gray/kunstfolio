<?php

namespace App\Http\Controllers;

class ApiController extends Controller
{
    public function getUserCalendarEvents(){
        $calenderEvents = request()->user()->calendarEvents()->get();
        return response()->json($calenderEvents);
    }
}
