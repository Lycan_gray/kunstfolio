<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassCode extends Model
{
    public function announcements() {
        return $this->hasMany('App\Announcements', 'class_id');
    }
    public function assignments() {
        return $this->hasMany('App\Assignment', 'class_id');
    }
}
