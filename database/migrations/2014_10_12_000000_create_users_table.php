<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phonenumber')->default('0612345678');
            $table->string('email')->unique();
            $table->string('user_slug')->unique();
            $table->string('address')->nullable();
            $table->string('housenumber')->default('1');
            $table->string('postalcode')->nullable();
            $table->string('city')->nullable();
            $table->string('avatar')->default('https://image.shutterstock.com/mosaic_250/0/0/490458556.jpg');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
